import fs from "fs-extra";
import { markdown } from "./utils/markdown.js";
import * as dotenv from "dotenv";
(async function () {
  console.log("Converting Markdown to HTML...");
  const EMAIL_ADDRESS: string = process.env.EMAIL_ADDRESS!;
  const COMPANY_NAME: string = process.env.COMPANY_NAME!;
  // markdown source
  const content = await fs.readFile("./content/index.md", "utf8");

  // converted to HTML
  const rendered = markdown.render(content, { NODE_ENV: "production" });

  const htmlFile = `<!DOCTYPE html>
  <html lang="en">
  <head>
  <meta charset="UTF-8" />
  <title>Convert Markdown to HTML with Node.js</title>
  <link rel="stylesheet" href="./default.css">
  </head>
  <body>
  ${rendered}
  </body>
  </html>`;

  await fs.mkdirs("./public");

  await fs.writeFile("./public/index.html", htmlFile, "utf8");

  const dataHtmlRaw: String = await fs.readFile('./public/index.html', 'utf8');
  const newDataHtml = dataHtmlRaw.replaceAll('$$EMAIL_ADDRESS$$', EMAIL_ADDRESS).replaceAll('$$COMPANY_NAME$$', COMPANY_NAME);

  await fs.writeFile("./public/index.html", newDataHtml, "utf8");




  await fs.copy(
    "./node_modules/highlight.js/styles/default.css",
    "./public/default.css",
    { overwrite: true }
  );

  console.log("HTML generated.");
})();